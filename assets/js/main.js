"use strict";

let _navigationScroll = function(element) {
    var id  = element.attr('href'),
        top = $(id).offset().top;
    $('body, html').animate({scrollTop: top}, 500);
};

let s, Site = {
    settings: {
        $preloader: $('#page-preloader'),
        $body: $("body"),
        $rooms: $('#rooms'),
        $checkboxBalcon: $('#checkbox-balcon'),
        $wrapPopup: $('#wrap-popup'),
        $whyWe: $('#why-we'),
        $up: $('#up')
    },
    init: function() {
        s = this.settings;
        this.bindActions();
        this.run();
    },

    loader: function() {
        setTimeout(function() {
            if(s.$preloader.hasClass('load')) {
                s.$preloader.removeClass('load');
                s.$body.css('overflow', 'auto');
            }
        }, 1000)
    },

    bindActions: function() {
        $(".js-icon").hover(function() {
            $(this).closest(".why-we-item").toggleClass("active");
        });

        $("#houses-content").on("click","a", function(event) {
            event.preventDefault();
            _navigationScroll($(this));
        });

        $("#change").on("click", function (event) {
            event.preventDefault();
            _navigationScroll($(this));
            localStorage.removeItem('house');
            localStorage.removeItem('flats');
        });

        $("select#select").change(function() {
            $(this).css('color','black');
        });
    },

    run: function() {
        this.startPlugins();
        this.upScroll();
        this.initHouses();
        this.renderOptionHouse();
        this.popup();
    },

    startPlugins: function() {
        $('#house-slider').slick({
            arrows:true,
            prevArrow:'<button type="button" class="slider-btn-prev"></button>',
            nextArrow:'<button type="button" class="slider-btn-next"></button>',
            centerMode:false,
            slidesToShow:1,
            slidesToScroll:1
        });

        $('input#phone').mask("(099) 999-99-99");
    },

    upScroll: function() {
        $(window).scroll(function () {
            if ($(this).scrollTop() >= (s.$whyWe.offset().top - (s.$whyWe.height()/2))) {
                s.$up.fadeIn();
            } else {
                s.$up.fadeOut();
            }
        });

        s.$up.click(function () {
            s.$body.animate({
                scrollTop: 0
            }, 500);
            return false;
        });
    },

    initHouses: function() {
        $.getJSON("data.json", function(obj) {
            let output = '';
            $.each(obj, function(index, val) {
                output += `
                    <div class="house col-xs-6 no-padding">
                        <a href="#choose-house" onclick="get_house(${index})" class="house-img col-xs-12 col-sm-12 col-md-6 no-padding">
                            <img src="${val.image}" alt="flat">
                            <span class="hover"></span>
                            <span class="coner"></span>
                        </a>
                        <div class="house-info col-xs-12 col-sm-12 col-md-6">
                            <h4 class="title">${val.title}</h4>
                            <p class="location">
                                ${val.city} <br>
                                ${val.street}
                            </p>
                            <span class="flats">${val.flats.length} Квартир</span>
                            <a href="#choose-house" class="link" onclick="get_house(${index})">Обрати квартиру</a>
                        </div>
                    </div>
                `
                $('#houses-content').html(output);
            })
        })
    },

    renderOptionHouse: function() {
        this._checkRooms();
        this._checkBalcon();
    },

    _checkRooms: function() {
        $('#opt-rooms').on('click', 'label', function() {
            let val = $(this).attr('for');

            // Add information in popup
            $("#popup-room-house").val(val + ' Кімнатна');

            let obj = localStorage.getItem('house');
            obj = JSON.parse(obj);

            let instance = '';
            let rooms = [];

            for(var i = 0; i < obj.flats.length; i++) {
                let room = obj.flats[i];
                if(val == room.rooms) {
                    rooms.push(room);

                    if(s.$checkboxBalcon.is(":checked") && room.balcon == true){
                        instance += `
                            <div class="numb-of-room">
                                <input type="radio" id="room-${room.numberOfFlat}" value="${room.numberOfFlat}" name="room">
                                <label for="room-${room.numberOfFlat}" onclick="get_room(${room.numberOfFlat})">${room.numberOfFlat}</label>
                            </div>
                        `
                    }
                    else if(!s.$checkboxBalcon.is(":checked") && room.balcon == false) {
                        instance += `
                            <div class="numb-of-room">
                                <input type="radio" id="room-${room.numberOfFlat}" value="${room.numberOfFlat}" name="room">
                                <label for="room-${room.numberOfFlat}" onclick="get_room(${room.numberOfFlat})">${room.numberOfFlat}</label>
                            </div>
                        `
                    }
                }
                s.$rooms.html(instance);
            }
            localStorage.setItem('flats', JSON.stringify(rooms));
        });
    },

    _checkBalcon: function() {
        s.$checkboxBalcon.on('change', function() {
            let _ths = $(this);
            let label = _ths.next('label');
            let instance = '';

            if(_ths.is(':checked')) {
                label.text('Так');

                // Add information in popup
                $("#popup-balcon-house").val('Так');

                let obj = localStorage.getItem('flats');
                obj = JSON.parse(obj);

                for(var i = 0; i < obj.length; i++) {
                    let room = obj[i];
                    if(room && room.balcon) {
                        instance += `
                            <div class="numb-of-room">
                                <input type="radio" id="room-${room.numberOfFlat}" value="${room.numberOfFlat}" name="room">
                                <label for="room-${room.numberOfFlat}" onclick="get_room(${room.numberOfFlat})">${room.numberOfFlat}</label>
                            </div>
                        `
                    }
                    s.$rooms.html(instance);
                }
            }
            else {
                label.text('Ні');

                // Add information in popup
                $("#popup-balcon-house").val('Ні');

                let obj = localStorage.getItem('flats');
                obj = JSON.parse(obj);

                for(var i = 0; i < obj.length; i++) {
                    let room = obj[i];
                    if(room && !room.balcon) {
                        instance += `
                            <div class="numb-of-room">
                                <input type="radio" id="room-${room.numberOfFlat}" value="${room.numberOfFlat}" name="room">
                                <label for="room-${room.numberOfFlat}" onclick="get_room(${room.numberOfFlat})">${room.numberOfFlat}</label>
                            </div>
                        `
                    }
                    s.$rooms.html(instance);
                }
            }
        })
    },

    popup: function() {
        $('#close-popup, #popup-send').on('click', function(event) {
            event.preventDefault();
            s.$wrapPopup.fadeOut(300);
            s.$body.css('overflow', 'auto');
        });

        $(document).click(function(event) {
            if( event.target == s.$wrapPopup[0]) {
                s.$wrapPopup.fadeOut(300);
                s.$body.css('overflow', 'auto');
            }
        });

        $('#open-popup').on('click', function(event) {
            event.preventDefault();
            s.$body.css('overflow', 'hidden');
            s.$wrapPopup.fadeIn(300);

            // $.ajax({
            //     type: "GET",
            //     url: "response.py",
            //     // dataType: "json",
            //     data: {
            //         houseName: $('#house-name').text()
            //     },
            //     success: function(data) {
            //         console.log(data)
            //     }
            // });
        });
    }
};

$.extend({
    Site: Site,
    s: s
});

window.onload = function() {
    Site.loader();
    Site.init();
}

function get_house(index) {
    let instance = '';
    $.getJSON("data.json", function(obj) {
        let house = obj[index];
        localStorage.setItem("house", JSON.stringify(house));

        $('#house-foto').attr('src', house.image);
        $('#house-name').text(house.title);

        // Add information in popup
        $("#popup-name-house").val(house.title);

        let room;
        for(var i = 0; i < house.flats.length; i++) {
            room = house.flats[i];
            if(room.rooms == 1 && room.balcon == false) {
                instance += `
                    <div class="numb-of-room">
                        <input type="radio" id="${room.numberOfFlat}" value="${room.numberOfFlat}" name="room">
                        <label for="${room.numberOfFlat}" onclick="get_room(${room.numberOfFlat})">${room.numberOfFlat}</label>
                    </div>
                `
            }
        }
        $('#rooms').html(instance);
    });
}

function get_room(value) {
    if(value) {
        $('#popup-number-of-room').val(value);
    }
}
